#!/usr/bin/env python3
# Reads serial from Arduino and uploads the data to a backend
# websockets is the better idea but ain't got the time

import serial
import requests

ser = serial.Serial('/dev/ttyACM0', 115200)
url = 'https://retricorne.appspot.com/up'

while True:
    line = ser.readline()
    print(line)
    if not line or not chr(line[0]).isdigit():
        continue
    nums = [float(a) for a in line.split()]
    p = {
        'a0': nums[0],
        'a1': nums[1],
        'a2': nums[2]
    }
    try:
        requests.get(url, params=p)
    except requests.exceptions.ConnectionError as e:
        print('upload fail')
